var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var config = require('./config');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var port = process.env.PORT || 8080;
mongoose.connect(config.database);
app.set('superSecret', config.secret);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan('dev'));
//app.use(require('./crawler'))
app.use(require('./routes/setup'));
app.use(require('./routes/api'));

app.listen(port);
console.log('Magic happens at http://localhost:' + port);

module.exports = app;
