var express = require('express');
var apiRoutes = express.Router();
var User = require('../models/user');
var Citation = require('../models/citations');
var Contexto = require('../models/contexto');
var app = express();
var mongoose = require('mongoose')
var bCrypt = require('bcrypt-nodejs')
var config = require('../config');
var jwt = require('jsonwebtoken');

app.set('superSecret', config.secret);

apiRoutes.post('/authenticate', function(req, res){
  User.findOne({
    email: req.body.email
  }, function(err, user){
    if(err) throw err;

    if(!user) {
      res.json({ success: false, message: 'Falha na autenticação. Usuário não achado' })
    }else if(user){
      var pass = bCrypt.hashSync(req.body.password, bCrypt.genSaltSync(10), null);
      var result = bCrypt.compareSync(req.body.password, user.password);
      if(result == false){
        res.json({ success: false, message: 'Falha de autenticação. Senha errada'});
      }else{
        var token = jwt.sign(user, app.get('superSecret'), {
          expiresIn: 60 * 60 * 24
        });
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
      }
    }
  });
});

apiRoutes.post('/signUp', function(req, res){

  var user = new User({
    name: req.body.name,
    estado: req.body.estado,
    oab_num: req.body.oab_num,
    email: req.body.email,
    password: bCrypt.hashSync(req.body.password, bCrypt.genSaltSync(10), null)
  });

  user.save(function(err){
    if(err) throw err;
    res.json({ success: true, message: 'Usuário criado com sucesso' });
  });
});

apiRoutes.post('/notification', function(req, res){
  if(req.body.flag == 1)
  {
     Contexto.find({ conteudo: { $regex: new RegExp(req.body.name,'ig') }}, function(err,doc){
       User.findOne({email: req.body.email }, function(err, usr){
         console.log(doc.length);
         for(var i = 0; i < doc.length; i++)
         {
           Citation.create({
               id_adv : usr._id,
               id_ctx : doc[i]._id,
               lido : true,
               notificado : true
           });
         }
       })
    });
    res.json({ success: true, message: 'Citações encontradas com sucesso' });
  }
  else{
    Contexto.find({ 'Diario' : req.body.diario }, function(err, ctxs){
      User.find({}, function(err, usrs){
        for(var i = 0; i < ctxs.length; i++){
          for(var j = 0; j < usrs.length; j++){
            if(ctxs[i].conteudo.match(new RegExp(usrs[j].name,'ig')))
            {
              Citation.create({
                  id_adv : usrs[j]._id,
                  id_ctx : ctxs[i]._id,
                  lido : false,
                  notificado : false
              });
            }
          }
        }
      })
    })
  }
  res.json({ success: true, message: 'Citações encontradas com sucesso' });
});

apiRoutes.post('/citacoes', function(req, res){
  User.findOne({ email: req.body.email }, function(err, usr){
    Citation.find({ id_adv : usr._id }, function(err, ctx){
      var array = [];
      for(var i = 0; i < ctx.length; i++){
        array.push(ctx[i].id_ctx);
      }
      if(ctx.length == 0)
        res.json({success: false, message: 'não foram achadas citacoes sobre você'});
      Contexto.find({_id : {$in : array }}, function(err, contextos){
        res.json(contextos);
      })
    })
  })
});

apiRoutes.use(function(req, res, next){

  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  if(token){
    jwt.verify(token, app.get('superSecret'),function(err, decoded){
      if(err) {
        return res.json({ success: false, message: 'Falha para autenticar o token' });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  }else {
    return res.status(403).send({
      success: false,
      message: 'No token provide'
    });
  }

});
module.exports = apiRoutes;
