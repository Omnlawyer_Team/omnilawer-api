var Crawler = require('crawler');
var fs = require('fs');
var http = require('http');
var actual_dj_number = 8248;
var CronJob = require('cron').CronJob;

let crawler = new Crawler({
    jQuery:false,
    callback:function(err, res, done){
      if(err){
        console.error(err.stack);
      }else{
        var url_send = res.request.uri.href;
        var url_split = url_send.split("/");
        var file = fs.createWriteStream(url_split[5]);
        var request = http.get(url_send, function(response) {
          response.pipe(file);
        });
      }

      done();
    }
});

var job = new CronJob('00 16 8 * * 1-5', function() {
  var temp_dj = fs.readFileSync('last_dj');
  crawler.queue("http://www.tjpi.jus.br/diarioeletronico/public/dj170714_" + temp_dj + ".pdf");
  fs.writeFileSync('last_dj', ++temp_dj);
}, null,true,'America/Fortaleza')
