var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('contexto', new Schema({
    titulo: String,
    diario: String,
    conteudo: String,
    secao: String,
    tag: String
}).index({ "conteudo" : 'text' }));
