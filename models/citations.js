var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Citacao', new Schema({
  id_adv: String,
  id_ctx: String,
  lido : Boolean,
  notificado: Boolean
}));
