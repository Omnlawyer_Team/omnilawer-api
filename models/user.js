var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('User', new Schema({
  name: String,
  password: String,
  estado: String,
  oab_num: String,
  email: String
}).index({ name: 'text' }));
